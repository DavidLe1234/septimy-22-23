## Rozklad čísla na prvočinitele

Deadline: 27.10.2022

### Popis:

Složené číslo je přirozené číslo, které má alespoň 3 různé dělitele
(tzn. alespoň jednoho dalšího dělitele kromě jedné a sebe sama).

Každé složené číslo lze zapsat jako součin prvočísel – tzv. kanonický rozklad čísla na prvočinitele.

### Zadaní:
Od uživatele získáte celé číslo, které rozložíte na součin prvočísel.


pr.:

Zadejte cislo
10

Vysledek je
2 * 5 * 1


Zadejte cislo
20

Vysledek je
2 * 2 * 5 * 1
