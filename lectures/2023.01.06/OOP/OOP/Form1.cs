﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();

        Person karel;
        Teacher david;
        Student marcel;

        Person[] people;

        public Form1()
        {
            InitializeComponent();
        }

        //Zavolá se před prvním zobrazením formuláře.
        private void Form1_Load(object sender, EventArgs e)
        {
            //pes
            Dog dog = new Dog("Zeryk");
            ICommunicable dog2 = new Dog("Dasenka");
            
            MessageBox.Show(dog.Communicate());
            MessageBox.Show(dog2.Communicate());
            
            //overload
            MessageBox.Show(dog.Run());
            MessageBox.Show(dog.Run(50));

            //override
            Animal dogA = new Dog("Maxipes");
            MessageBox.Show(dogA.Move());
            //MessageBox.Show(dogA.MoveFromBase());




            people = new Person[3];
            karel = new Person(rnd.Next(1, 200), rnd.Next(1, 200), "Karel");

            david = new Teacher(200, 50, "David");
            marcel = new Student(200, 200, "Marcel");

            people[0] = david;
            people[1] = marcel;
            people[2] = karel;

            //pes


        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            //vytvorime instanci pepi
            //new, konstruktor, konkretni objekt
            Person pepa = new Person(20, 20, "Pepa");
            pepa.Draw(kp);

            //vykresli deset panacku
            //for (int i = 0; i < 10; i++)
            //{
            //    Person person = new Person(rnd.Next(1, 600), rnd.Next(1, 300));
            //    person.Draw(kp);
            //}

            //vytvor karla jako clenskou promennou
            //pri stisknuti mezerniku se pohne o x a y
            karel.Draw(kp);

            //pri stisku jiny klavesy M pozdravi

            //ukaz dedeni - student rekne ze je student a jeste barva
            //this predstavuje aktualni tridu
            //base rodice
            david.Draw(kp);
            marcel.Draw(kp);

            //ulozit do pole


            //menit nohy prii pohybu

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                karel.Move(25, 0);
            }
            if (e.KeyCode == Keys.M)
            {
                karel.SayHi();
            }
            if (e.KeyCode == Keys.J)
            {
                /*karel.SayJob();
                marcel.SayJob();
                david.SayJob();
                */

                foreach (Person item in people)
                {
                    item.SayJob();
                }

            }

            Refresh();
        }
    }
}