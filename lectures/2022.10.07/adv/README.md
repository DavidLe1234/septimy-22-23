# Pi
Cílem úlohy je aproximovat číslo Pi pomocí částečných součtů dvou nekonečných řad.

## Gregoryho-Leibnizova řada
Vytvořte funkci s názvem LeibnizPi s jedním parametrem (určujícím počet členů), jež sečte daný počet členů Gregoryho-Leibnizovy řady:

![alt text](./greg.png?raw=true)

Tzn. volání funkce LeibnizPi(1) vrátí 4.0, LeibnizPi(2) vrátí 2.666666, atd.

## Nilakantova řada
Vytvořte obdobnou funkci NilakanthaPi (také s jedním parametrem) sčítající řadu:

![alt text](./nilakantova.png?raw=true)

Tzn. volání funkce NilakanthaPi(1) vrátí 3.0, NilakanthaPi(2) vrátí 3.166666, atd.
