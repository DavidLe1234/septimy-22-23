## "Šifrování"

Deadline: 2.12.2022

1) Vytvoříte in.txt soubor s nějakým čitelným textem.
2) Napíšete program, který na vstupu příjme číslo.
3) O to číslo posunete každý znak vašeho textu – zašifrujete.
4) Zašifrovaný text uložíte do souboru out.txt.

Do gitu nahrajte i textový soubory in.txt a out.txt.

