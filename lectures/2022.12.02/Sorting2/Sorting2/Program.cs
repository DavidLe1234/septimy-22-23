﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] pole = new int[20];
            Random rnd = new Random();
            for (int i = 0; i < 20; i++)
            {
                pole[i] = rnd.Next(0, 20);
            }
            printArray(pole);
            printArray(reverseArray(pole));
            Console.WriteLine();
            SelectionSort(pole);
            printArray(pole);





            Console.ReadKey();
        }

        // O(n^2)
        // nestabilni   5(a) 3 4 5(b) 2    6 8
        //              2    3 4 5(b) 5(a) 6 8
        // neprirozeny
        static void SelectionSort(int[] array)
        {

            int min, temp;
            for (int i = 0; i < array.Length; i++)
            {
                // najdi min
                min = i;
                for (int j = i; j < array.Length; j++)
                {
                    if(array[min] > array[j])
                    {
                        min = j;
                    }
                }

                //swap
                temp = array[i];
                array[i] = array[min];
                array[min] = temp;
            }
        }



        static int[] reverseArray(int[] array)
        {
            int[] reversedArray = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                reversedArray[array.Length - i - 1] = array[i];
            }


            return reversedArray;
        }


        static void printArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

    }
}
