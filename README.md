# Septimy 22-23

GYMVOD programovací seminář Septimy 2022/2023

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php, javascript, python


### TEST
- 11.11.2022 ze všeho!


### Online



### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/


### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord - https://discord.gg/ZwSBbRBnW9
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. 
- Dvě až tři písemky za pololetí (teoretická a praktická část).
- Při hodině budete spolupracovat.

### Harmonogram 

| Datum | Učitel | Téma | Přednášky | Úkol |
| --- | --- | --- | --- | --- |
| 2.9.2022 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; basic úloha | [lec01](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.02/uvodni_prezentace.pdf) | |
| 9.9.2022 | David | | | |
| 16.9.2022 | David | GIT, historie, lokální práce | [lec02](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.16/git-uvod.pdf) | |
| 23.9.2022 | David | Gitlab, Progamovací jazyky, kvadratická rovnice  | [lec03](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.23/) | [hw01](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.23/hw)|
| 23.9.2022 | David | Datové typy | [lec04](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.30/) | [hw02](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.30/hw)|
| 7.10.2022 | David | Procvičování | [lec05](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.07/) | |
| 14.10.2022 | David | Základní struktury | [lec06](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.14/) | |
| 21.10.2022 | David | Cyklus a rekurze | [lec07](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.21/) | [hw03](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.21/hw) |
| 28.10.2022 | David | Volno | | |
| 04.11.2022 | David | Rekurze a seznamy| [lec08](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.04/) | [hw04](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.04/hw) |
| 11.11.2022 | David | Test | | |
| 18.11.2022 | David | Volno | | |
| 25.11.2022 | David | String a soubory | [lec09](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.25/) | [hw05](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.25/hw) |
| 02.12.2022 | David | Sorting | [lec10](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.02/) | [hw06](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.02/hw) |
| 09.12.2022 | David | Sorting  + gui| [lec11](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.09/) |  |
| 16.12.2022 | David | OOP | [lec12](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.16/) | [hw08](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.16/hw)  |
| 06.01.2023 | David | OOP 2 | [lec13](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.01.06/) |  |

